---
title: Week 8 - dinsdag
date: 2018-10-30
---
Vandaag ben ik samen met de groep voornamelijk bezig geweest met het geven van georganiseerde feedback, hierbij heb ik de vragenlijst samenwerking team en het peerfeedback formulier ingevuld. Andere teamleden hebben voor mij hetzelfde gedaan.

![](https://i.imgur.com/hqepbj3.jpg)

![](https://i.imgur.com/pGWVLcB.jpg)

Daarnaast is er hard gewerkt aan de presentatie die morgen staat ingepland, hierbij zijn de rollen en gespreksonderwerpen verdeeld. Het is er op neergekomen dat ieder zijn eigen werk presenteert, omdat diegene natuurlijk het beste over zijn eigen werk kan vertellen. Verder is de presentatie ingedeeld op volgorde voor conceptontwikkelijng, je begint bij stap en en eindigt met de concepten.