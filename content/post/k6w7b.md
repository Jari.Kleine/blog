---
title: Week 7 - vrijdag
date: 2018-10-19
---
Vandaag heb ik hard gewerkt aan het onderzoeksverslag, waarvoor vandaag de deadine is. Het duurde voor sommige aanpassingen van de vorige versie van het verslag erg lang om af te rijgen, waardoor het erg krap werd voor de deadline. Zelf ben ik verrantwoordelijk geweest, net als de vorige versie, voor de uitstraling van het verslag.