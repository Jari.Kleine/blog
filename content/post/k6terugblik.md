---
title: Terugblik
date: 2018-11-05
---
Over het algemeen ben ik tevreden met wat ik bereikt heb. De samenwerking heeft niet altijd soepeltjes verlopen, maar alle deelproducten zijn wel in orde. Zelf heb ik gedaan wat ik gedaan wou hebben.

In relatie tot het team zijn er verbeterpunten: te laat komen is eerder de norm dan een uitzondering bij sommige teamleden, de communicatie onder elkaar is mager en het is vaak onduidelijk wat de verwachtingen zijn van elkaar.