---
title: Week 5
date: 2018-05-28
---
Erg veel gehaast vanwege de expo die ons al in de nek aan het hijgen was. Mijn deel van de schermen voor het high-fid prototype gemaakt. Ook heb ik de laatste workshop van HTML/CSS gevolgd, de derde. Een erg interessante workshop waar we het limiet van CSS hebben opgezocht. Ook heb ik de workshop empathie gevolgd. 
Misschien iets te laat nu dat ik de interviews en persona al heb gemaakt, maar nog steeds handig voor mijn STARRTs. Verder hebben we de expo gehad, ik was erg tevreden met de uitwerking en het eindresultaat. Zelf probeerde ik minder bij de stand te blijven, om de andere teamgenoten de ruimte te geven om te presenteren voor hun competentie. De expo was een geslaagde avond.