---
title: Terugblik
date: 2018-05-28
---
Dit kwartaal war erg hectisch, heel veel gedaan. Ik ben ook alle vijf wegen vijf dagen op school geweest, zodat ik alles goed op tijd af kon krijgen. Niet alles liep op rolletjes, maar dat is nou niet altijd zo natuurlijk.
Vergeleken met dc3 was het teamwerk vele malen beter verlopen, deels vanwege het al bekende groepje en deels vanwege de naderende deadline voor je competenties.
Ook heb ik de feedback om meer workshops te volgens van harte genomen en meer workshops gevolgd dan de andere drie kwartalen bij elkaar.
![](https://i.imgur.com/jMN5Ybc.jpg)