---
title: Peerfeedback
date: 2018-01-02
---
Voor- en achterkant van de peerfeedback.

Voorkant peerfeedback
![](https://i.imgur.com/7D3RgVd.jpg)

Achterkant peerfeedback
![](https://i.imgur.com/NJsuKxq.jpg)