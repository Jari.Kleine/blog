---
title: Peerfeedback
date: 2017-10-25
---
Voor- en achterkant van het eerste- en tweede peerfeedback moment.

Voorkant peerfeedback moment 1
![](https://i.imgur.com/GoB0Brx.jpg)

Achterkant peerfeedback moment 1
![](https://i.imgur.com/wKFMb5z.jpg)

Voorkant peerfeedback moment 2
![](https://i.imgur.com/ZuR9GFH.jpg)

Achterkant peerfeedback moment 2
![](https://i.imgur.com/KqIVCSl.jpg)